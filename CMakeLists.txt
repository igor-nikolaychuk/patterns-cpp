cmake_minimum_required(VERSION 3.15)
project(patterns_cpp)

set(CMAKE_CXX_STANDARD 17)

add_subdirectory(third-party)
add_subdirectory(src)
