#include "Memento.h"
#include <gtest/gtest.h>

using namespace Memento;

TEST(Memento, UsageSample)
{
    ArticleEditor editor;

    editor.Edit([&](Article& article){
        article.setText("Hello");
        article.setTitle("Greeting");
    });

    EXPECT_EQ("Greeting", editor.GetArticle().getTitle());
    EXPECT_EQ("Hello", editor.GetArticle().getText());

    editor.Edit([&](Article& article){
        article.setText("Good bye");
        article.setTitle("Parting");
    });

    EXPECT_EQ("Parting", editor.GetArticle().getTitle());
    EXPECT_EQ("Good bye", editor.GetArticle().getText());

    editor.Undo();

    EXPECT_EQ("Greeting", editor.GetArticle().getTitle());
    EXPECT_EQ("Hello", editor.GetArticle().getText());
}
