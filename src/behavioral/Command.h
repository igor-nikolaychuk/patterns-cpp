#ifndef PATTERNS_CPP_COMMAND_H
#define PATTERNS_CPP_COMMAND_H

namespace Command
{
    class BankAccount
    {
    public:
        int GetAmount() const
        {
            return amount_;
        }

        void Deposit(size_t sum)
        {
            amount_ += sum;
        }

        void Withdraw(size_t sum)
        {
            amount_ -= sum;
        }

    public:
        int amount_ = 0;
    };

    class ICommand
    {
    public:
        virtual void Do() = 0;
        virtual void Undo() = 0;
        virtual ~ICommand() = default;
    };

    using ICommandPtr = std::unique_ptr<ICommand>;


    class DepositCommand: public ICommand
    {
    public:
        DepositCommand(BankAccount& bankAccount, size_t amount):
            bankAccount_(bankAccount), amount_(amount) {}

        void Do() override
        {
            bankAccount_.Deposit(amount_);
        }

        void Undo() override
        {
            bankAccount_.Withdraw(amount_);
        }

    private:
        BankAccount& bankAccount_;
        size_t amount_;
    };

    class WithdrawCommand: public ICommand
    {
    public:
        WithdrawCommand(BankAccount& bankAccount, size_t amount):
                bankAccount_(bankAccount), amount_(amount) {}

        void Do() override
        {
            bankAccount_.Withdraw(amount_);
        }

        void Undo() override
        {
            bankAccount_.Deposit(amount_);
        }

    private:
        BankAccount& bankAccount_;
        size_t amount_;
    };

};

#endif //PATTERNS_CPP_COMMAND_H
