#include "State.h"
#include <gtest/gtest.h>

using namespace State;

TEST(State, UsageExample)
{

}

Player::Player(): currentState_(new StoppedPlayerState(*this)), displayText_() { }

void Player::Play() {
    currentState_->Play();
}

void Player::Pause() {
    currentState_->Pause();
}

void Player::Stop() {
    currentState_->Stop();
}

std::string_view Player::DisplayText() const {
    return displayText_;
}

Player::StoppedPlayerState::StoppedPlayerState(Player &player) : player_(player) {}

void Player::StoppedPlayerState::Play() {
    player_.displayText_ = "Playing";
    player_.currentState_ = std::make_unique<PlayingPlayerState>(player_);
}


Player::PlayingPlayerState::PlayingPlayerState(Player &player): player_(player) {}

void Player::PlayingPlayerState::Pause() {
    player_.displayText_ = "Paused";
    player_.currentState_ = std::make_unique<PausedPlayerState>(player_);
}

void Player::PlayingPlayerState::Stop() {
    player_.displayText_ = "Stopped";
    player_.currentState_ = std::make_unique<StoppedPlayerState>(player_);
}

Player::PausedPlayerState::PausedPlayerState(Player &player): player_(player) { }

void Player::PausedPlayerState::Play() {
    player_.displayText_ = "Playing";
    player_.currentState_ = std::make_unique<PlayingPlayerState>(player_);
}

void Player::PausedPlayerState::Stop() {
    player_.displayText_ = "Stopped";
    player_.currentState_ = std::make_unique<StoppedPlayerState>(player_);
}
