#ifndef PATTERNS_CPP_CHAINOFRESPONSIBILITY_H
#define PATTERNS_CPP_CHAINOFRESPONSIBILITY_H

#include <memory>
#include <string>
#include <map>
#include <ctime>

struct HttpRequest
{
    std::string path;
    std::map<std::string, std::string> headers;
    std::string body;
};

class IHttpResponseWriter
{
public:
    virtual void WriteStatus(int status) = 0;
    virtual void WriteBody(std::string_view data) = 0;
    virtual void Flush() = 0;
};

class IHttpRequestHandler
{
public:
    virtual void Handle(const HttpRequest& request, IHttpResponseWriter& writer) = 0;
};

class AuthenticationHandler: public IHttpRequestHandler
{
public:
    explicit AuthenticationHandler(std::unique_ptr<IHttpRequestHandler> nextHander)
     : nextHandler_(std::move(nextHander)) {}

    void Handle(const HttpRequest &request, IHttpResponseWriter &writer) override
    {
        auto reqToken = request.headers.find("token");
        if (reqToken == request.headers.cend() || reqToken->second != "SECRET")
        {
            writer.WriteStatus(403);
            writer.WriteBody("Access denied");
            writer.Flush();
            return;
        }

        if (nextHandler_)
        {
            nextHandler_->Handle(request, writer);
        }
    }

    std::unique_ptr<IHttpRequestHandler> nextHandler_;
};

class GreetingRequestHandler: public IHttpRequestHandler
{
public:
    explicit GreetingRequestHandler(std::unique_ptr<IHttpRequestHandler> nextHander)
     : nextHandler_(std::move(nextHander)) {}

    void Handle(const HttpRequest &request, IHttpResponseWriter &writer) override
    {
        writer.WriteStatus(200);
        writer.WriteBody("Welcome!");

        if (nextHandler_)
        {
            nextHandler_->Handle(request, writer);
        }
    }

    std::unique_ptr<IHttpRequestHandler> nextHandler_;
};

class TimestampRequestHandler: public IHttpRequestHandler
{
public:
    explicit TimestampRequestHandler(std::unique_ptr<IHttpRequestHandler> nextHander)
     : nextHandler_(std::move(nextHander)) {}

    void Handle(const HttpRequest &request, IHttpResponseWriter &writer) override
    {
        writer.WriteBody("Current timestamp is: ");
        std::time_t result = std::time(nullptr);
        writer.WriteBody(std::asctime(std::localtime(&result)));

        if (nextHandler_)
        {
            nextHandler_->Handle(request, writer);
        }
    }

    std::unique_ptr<IHttpRequestHandler> nextHandler_;
};

#endif //PATTERNS_CPP_CHAINOFRESPONSIBILITY_H