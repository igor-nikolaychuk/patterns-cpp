#ifndef PATTERNS_CPP_STATE_H
#define PATTERNS_CPP_STATE_H

#include <memory>
#include <string>

namespace State
{
    class Player
    {
    public:
        Player();

        void Play();
        void Pause();
        void Stop();

        std::string_view DisplayText() const;

    private:
        class IPlayerState
        {
        public:
            virtual void Play()  = 0;
            virtual void Pause() = 0;
            virtual void Stop()  = 0;

            ~IPlayerState() = default;
        };

        class StoppedPlayerState;
        class PlayingPlayerState;
        class PausedPlayerState;

    private:
        std::unique_ptr<IPlayerState> currentState_;
        std::string displayText_;
    };

    class Player::StoppedPlayerState: public Player::IPlayerState
    {
    public:
        explicit StoppedPlayerState(Player& player);

        void Play() override;
        void Pause() override { }
        void Stop() override { }

    private:
        Player& player_;
    };

    class Player::PlayingPlayerState: public Player::IPlayerState
    {
    public:
        explicit PlayingPlayerState(Player& player);

        void Play() override { };
        void Pause() override;
        void Stop() override;

    private:
        Player& player_;
    };

    class Player::PausedPlayerState: public Player::IPlayerState
    {
    public:
        explicit PausedPlayerState(Player& player);

        void Play() override;
        void Pause() override {};
        void Stop() override;

    private:
        Player& player_;
    };

}

#endif //PATTERNS_CPP_STATE_H