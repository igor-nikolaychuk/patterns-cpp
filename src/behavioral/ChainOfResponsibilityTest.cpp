#include <memory>

#include <gtest/gtest.h>
#include "ChainOfResponsibility.h"

std::unique_ptr<IHttpRequestHandler> BuildHandlerChain()
{
     return std::make_unique<AuthenticationHandler>(
        std::make_unique<GreetingRequestHandler>(
            std::make_unique<TimestampRequestHandler>(nullptr)
        )
     );
}

class StubResponseWriter: public IHttpResponseWriter
{
public:
    void WriteStatus(int stat) override
    {
        status = stat;
    }

    void WriteBody(std::string_view data) override
    {
        body += data;
    }

    void Flush() override
    {}

    int status = 0;
    std::string body;
};

TEST(ChainOfResponsibility, UnauthenticatedRequest)
{
    auto chain = BuildHandlerChain();
    StubResponseWriter stubResponseWriter;

    HttpRequest request {};

    chain->Handle(request, stubResponseWriter);

    EXPECT_EQ("Access denied", stubResponseWriter.body);
    EXPECT_EQ(403, stubResponseWriter.status);
}

TEST(ChainOfResponsibility, RegularRequest)
{
    auto chain = BuildHandlerChain();
    StubResponseWriter stubResponseWriter;

    HttpRequest request {};
    request.headers["token"] = "SECRET";

    chain->Handle(request, stubResponseWriter);

    EXPECT_TRUE(stubResponseWriter.body.find("Welcome") != std::string::npos);
    EXPECT_TRUE(stubResponseWriter.body.find("Current timestamp") != std::string::npos);
    EXPECT_EQ(200, stubResponseWriter.status);
};;
