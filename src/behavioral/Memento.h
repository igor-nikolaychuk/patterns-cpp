#ifndef PATTERNS_CPP_MEMENTO_H
#define PATTERNS_CPP_MEMENTO_H

#include <string>
#include <utility>
#include <functional>
#include <stack>

namespace Memento
{
    class ArticleMemento
    {
    public:
        ArticleMemento(std::string  title, std::string  text)
            : title_(std::move(title)), text_(std::move(text)) { }

    private:
        friend class Article;

        std::string title_;
        std::string text_;
    };

    // Originator
    class Article
    {
    public:
        const std::string& getTitle() const {
            return title_;
        }

        void setTitle(const std::string &title) {
            title_ = title;
        }

        const std::string& getText() const {
            return text_;
        }

        void setText(const std::string &text) {
            text_ = text;
        }

    public:
        ArticleMemento SaveToMemento() const
        {
            return {title_, text_};
        }

        void RestoreFromMemento(const ArticleMemento& memento)
        {
            title_ = memento.title_;
            text_ = memento.text_;
        }

    private:
        std::string title_;
        std::string text_;
    };

    // Caretaker
    class ArticleEditor
    {
    public:
        const Article& GetArticle()
        {
            return article_;
        }

        void Edit(const std::function<void(Article&)>& editor)
        {
            history_.push(article_.SaveToMemento());
            editor(article_);
        }

        void Undo()
        {
            if (!history_.empty())
            {
                article_.RestoreFromMemento(history_.top());
                history_.pop();
            }
        }

    private:
        Article article_;
        std::stack<ArticleMemento> history_;
    };
}

#endif //PATTERNS_CPP_MEMENTO_H
