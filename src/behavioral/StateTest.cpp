#include "State.h"
#include <gtest/gtest.h>

using namespace State;

TEST(State, UsageSample)
{
    Player player;
    player.Play();
    EXPECT_EQ("Playing", player.DisplayText());
    player.Stop();
    EXPECT_EQ("Stopped", player.DisplayText());
    player.Play();
    EXPECT_EQ("Playing", player.DisplayText());
    player.Pause();
    EXPECT_EQ("Paused", player.DisplayText());
}