#include <memory>

#include <gtest/gtest.h>
#include "Command.h"

using namespace Command;

TEST(Command, TransactionSequence)
{
    BankAccount bankAccount;

    EXPECT_EQ(bankAccount.GetAmount(), 0);

    std::vector<ICommandPtr> commands;
    commands.push_back(ICommandPtr{ new DepositCommand(bankAccount, 100) });
    commands.push_back(ICommandPtr{ new WithdrawCommand(bankAccount, 50) });
    commands.push_back(ICommandPtr{ new DepositCommand(bankAccount, 100) });
    commands.push_back(ICommandPtr{ new DepositCommand(bankAccount, 100) });


    for(ICommandPtr& c: commands)
    {
        c->Do();
    }

    EXPECT_EQ(bankAccount.GetAmount(), 250);

    for(ICommandPtr& c: commands)
    {
        c->Undo();
    }

    EXPECT_EQ(bankAccount.GetAmount(), 0);

};
