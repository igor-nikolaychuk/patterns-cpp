#ifndef PATTERNS_CPP_OBSERVER_H
#define PATTERNS_CPP_OBSERVER_H

#include <any>
#include <vector>

using namespace std;
class Person;

class PersonObserver
{
    virtual ~PersonObserver() = default;

    virtual void personChanged(Person& p, //Object reference
        const string& propertyName,           //Name of changed property
        const any& newValue) = 0;              //New property value
};

class Person {
private:
    int age;
    vector<shared_ptr<PersonObserver>> listeners;

private:
    void notify(const string& propertyName, const any& newValue) {
        for(const auto l&: listeners) {
            l->personChanged(*this, propertyName, newValue);
        }
    }

public:
    explicit Person(int age) : age(age) {}

    int getAge() const {
        return age;
    }

    void setAge(int age) {
        Person::age = age;
        notify("age", std::make_any<std::string>(age));
    }

    //Observable subscription method accepting a listener
    void subscribe(shared_ptr<PersonObserver> listener) {
        //Just add the listener to the vector
        listeners.push_back(move(listener));
    }
};

/* Actual implementation of abstract person observer implementing
 * the virtual notification handler method "personChanged" */
struct ConsoleListener: public  PersonObserver {
    void personChanged(Person &p, const string &propertyName, const any newValue) override {
        // Just print the updated property and it's value
        std::cout << "Person's " << propertyName << " has been changed to ";
        if(propertyName == "age")
            std::cout << any_cast<int>(newValue);

        std::cout << std::endl;
    }
};

#endif //PATTERNS_CPP_OBSERVER_H
