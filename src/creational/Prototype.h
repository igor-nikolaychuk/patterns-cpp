#ifndef PATTERNS_CPP_PROTOTYPE_H
#define PATTERNS_CPP_PROTOTYPE_H

#include <string>

namespace Prototype
{
    struct Employee
    {
        std::string name;
        std::string surname;
        std::string companyName;
        std::string companyAddress;

        Employee(const std::string &name,
                 const std::string &surname,
                 const std::string &companyName,
                 const std::string &companyAddress) :
                 name(name), surname(surname),
                 companyName(companyName), companyAddress(companyAddress)
        {
        }

        Employee(std::string name, std::string surname, const Employee& prototype) :
            name(std::move(name)),
            surname(std::move(surname)),
            companyName(prototype.companyName),
            companyAddress(prototype.companyAddress)
        { }
    };


    class AprioritEmpoyeeFactory
    {
    public:
        AprioritEmpoyeeFactory()
        : mainOfficeEmployeePrototype_{"", "", "Apriorit", "Office 1"},
          auxOfficeEmployeePrototype_{"", "", "Apriorit", "Office 2"}
        { }

        Employee MakeMainOfficeEmployee(std::string name, std::string surname)
        {
            return Employee(std::move(name), std::move(surname), mainOfficeEmployeePrototype_);
        }

        Employee MakeAuxOfficeEmployee(std::string name, std::string surname)
        {
            return Employee(std::move(name), std::move(surname), auxOfficeEmployeePrototype_);
        }

    private:
        Employee mainOfficeEmployeePrototype_;
        Employee auxOfficeEmployeePrototype_;
    };

};

#endif //PATTERNS_CPP_PROTOTYPE_H
