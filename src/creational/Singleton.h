#ifndef PATTERNS_CPP_SINGLETON_H
#define PATTERNS_CPP_SINGLETON_H

#include <string>

namespace Singleton
{
    class Configuration
    {
    public:
        std::string port;
        std::string fileName;
        //...
    public:
        static Configuration& instance()
        {
            static Configuration instance;

            return instance;
        };
    };
};

#endif //PATTERNS_CPP_SINGLETON_H
