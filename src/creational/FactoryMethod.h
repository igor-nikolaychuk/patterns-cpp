#ifndef PATTERNS_CPP_FACTORYMETHOD_H
#define PATTERNS_CPP_FACTORYMETHOD_H

#include <vector>
#include <string>
#include <memory>

namespace FactoryMethod
{
    using TableRow = std::vector<std::string>;
    using Table    = std::vector<TableRow>;

    class IDatabaseEngine
    {
    public:
        virtual void Connect() = 0;
        virtual Table ExecuteQuery(const std::string& query) = 0;
        virtual ~IDatabaseEngine() = default;
    };

    struct Car
    {
        std::string vendor;
        std::string model;
    };

    class CarRepository
    {
    public:
        std::vector<Car> GetCars()
        {
            std::vector<Car> result;

            auto engine = DatabaseEngine();
            engine->Connect();
            Table rows = engine->ExecuteQuery("SELECT * FROM cars;");
            for(const TableRow& r: rows)
            {
                if (r.size() == 2)
                {
                    result.emplace_back(r[0], r[1]);
                }
            }
        }

    protected:
        // Factory method
        virtual std::unique_ptr<IDatabaseEngine> DatabaseEngine() = 0;
    };

    class MySqlDatabaseEngine: public IDatabaseEngine
    {
    public:
        void Connect() override {
            // implementation details
        }

        Table ExecuteQuery(const std::string &query) override {
            return FactoryMethod::Table();
        }
    };

    class MySqlDatabaseCarRepository: CarRepository
    {
        std::unique_ptr<IDatabaseEngine> DatabaseEngine() override {
            return std::make_unique<MySqlDatabaseEngine>();
        }
    };
}

#endif //PATTERNS_CPP_FACTORYMETHOD_H