#ifndef PATTERNS_CPP_NESTEDBUILDER_H
#define PATTERNS_CPP_NESTEDBUILDER_H

#include <vector>
#include <string>
#include <set>

namespace NestedBuilder
{
    struct EmailAttachment
    {
        std::string mimeType;
        std::string content;
    };

    class EmailBuilder;

    struct Email
    {
        std::string subject;
        std::set<std::string> receivers;
        std::string body;
        std::vector<EmailAttachment> attachments;

        static EmailBuilder Make(Email& email);
    };

    class EmailAttachmenBuilder;

    class EmailBuilder
    {
    public:
        explicit EmailBuilder(Email& email)
            : email_(email) {}

        EmailBuilder& SetSubject(std::string subject)
        {
            email_.subject = std::move(subject);

            return *this;
        }

        EmailBuilder& AddReceiver(std::string receiver)
        {
            EnsureEmailAddressValid(receiver);

            if(email_.receivers.find(receiver) != email_.receivers.end())
            {
                email_.receivers.insert(std::move(receiver));
            }

            return *this;
        }

        EmailAttachmenBuilder AddAttachment();

    protected:
        static void EnsureEmailAddressValid(std::string_view Email)
        {
            // TODO: perform better validation logic
            if (Email.find('@') == std::string_view::npos)
            {
                throw std::runtime_error("Email address is not valid");
            }
        }

        Email& email_;
    };

   class EmailAttachmenBuilder: public EmailBuilder
    {
    public:
       EmailAttachmenBuilder(Email& email, EmailAttachment& targetAttachment)
        : EmailBuilder(email), targetAttachment_(targetAttachment) {}

       EmailAttachmenBuilder& SetMimeType(std::string type)
        {
            // TODO: implement type check

            targetAttachment_.mimeType = std::move(type);
            return *this;
        }

       EmailAttachmenBuilder& SetContent(std::string content)
        {
            targetAttachment_.content = std::move(content);
            return *this;
        }

    private:
        EmailAttachment& targetAttachment_;
    };

    EmailAttachmenBuilder EmailBuilder::AddAttachment()
    {
        email_.attachments.emplace_back();
        return EmailAttachmenBuilder{email_, email_.attachments.back()};
    }

    EmailBuilder Email::Make(Email& email)
    {
        return EmailBuilder(email);
    }
};

#endif //PATTERNS_CPP_NESTEDBUILDER_H
