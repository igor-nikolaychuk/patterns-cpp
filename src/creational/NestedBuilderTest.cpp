#include <gtest/gtest.h>
#include "NestedBuilder.h"

using namespace NestedBuilder;

TEST(NestedBuilder, TwoAttachmentEmail)
{
    // Without builder
    Email email;

    email.receivers.insert("test@mail.com");
    email.subject = "hello world";

    EmailAttachment a1;
    a1.mimeType = "text/html";
    a1.content = "attachment 1";

    email.attachments.push_back(std::move(a1));

    EmailAttachment a2;
    a2.mimeType = "text/css";
    a2.content = ".p {}";

    email.attachments.push_back(std::move(a2));

    // With builder
    Email email2;
    Email::Make(email2)
          .AddReceiver("test@mail.com")
          .SetSubject("Hello world")
          .AddAttachment()
            .SetMimeType("text/html")
            .SetContent("attachment 1")
          .AddAttachment()
            .SetMimeType("text/css")
            .SetContent(".p {}");
}

