#ifndef PATTERNS_CPP_FACTORYTEST_H
#define PATTERNS_CPP_FACTORYTEST_H

namespace Factory
{
    enum class CarType
    {
        Fast,
        Slow
    };

    class Car
    {
    public:
        virtual size_t MaxMph() const = 0;

        virtual ~Car() = default;

        static std::unique_ptr<Car> MakeCar(CarType carType);
    };

    class FastCar : public Car
    {
    public:
        size_t MaxMph() const override
        {
            return MAX_MPH;
        }

        inline static const size_t MAX_MPH = 300;
    };

    class SlowCar : public Car
    {
    public:
        size_t MaxMph() const override
        {
            return MAX_MPH;
        }

        inline static const size_t MAX_MPH = 120;
    };

    std::unique_ptr<Car> Car::MakeCar(CarType carType)
    {
        switch (carType)
        {
            case CarType::Fast:
                return std::make_unique<FastCar>();
            case CarType::Slow:
                return std::make_unique<SlowCar>();
            default:
                return nullptr;
        }
    }
}
#endif //PATTERNS_CPP_FACTORYTEST_H
