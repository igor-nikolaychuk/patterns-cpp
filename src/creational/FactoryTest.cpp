#include <gtest/gtest.h>
#include "FactoryTest.h"

using namespace Factory;

TEST(Factory, MakeFastCar)
{
    auto fastCarPtr = Car::MakeCar(CarType::Fast);
    EXPECT_EQ(fastCarPtr->MaxMph(), FastCar::MAX_MPH);
}

TEST(Factory, MakeSlowCar)
{
    auto fastCarPtr = Car::MakeCar(CarType::Slow);
    EXPECT_EQ(fastCarPtr->MaxMph(), SlowCar::MAX_MPH);
}

