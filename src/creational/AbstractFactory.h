#ifndef PATTERNS_CPP_ABSTRACTFACTORY_H
#define PATTERNS_CPP_ABSTRACTFACTORY_H

#include <cstddef>
#include <memory>

namespace AbstractFactory
{
    class IWarriorEnemy
    {
    public:
        virtual size_t MeleeAttack() = 0;
        virtual ~IWarriorEnemy() = default;
    };

    class IArcherEnemy
    {
    public:
        virtual size_t RangeAttack() = 0;
        virtual ~IArcherEnemy() = default;
    };

   class CommonWarrior: public IWarriorEnemy
    {
    public:
        size_t MeleeAttack() override
        {
            return ATTACK;
        }

        inline static const size_t ATTACK = 200;
    };

    class LegendaryWarrior: public IWarriorEnemy
    {
    public:
        size_t MeleeAttack() override
        {
            return ATTACK;
        }

        inline static const size_t ATTACK = 800;
    };

    class CommonArcher: public IArcherEnemy
    {
    public:
        size_t RangeAttack() override
        {
            return ATTACK;
        }

        inline static const size_t ATTACK = 200;
    };

    class LegendaryArcher: public IArcherEnemy
    {
    public:
        size_t RangeAttack() override
        {
            return ATTACK;
        }

        inline static const size_t ATTACK = 800;
    };

    class IEnemyFactory
    {
    public:
        virtual std::unique_ptr<IWarriorEnemy> MakeWarrior() = 0;
        virtual std::unique_ptr<IArcherEnemy> MakeArcher() = 0;

        virtual ~IEnemyFactory() = default;
    };

    class NormalDifficultyEnemyFactory: public IEnemyFactory
    {
    public:
        std::unique_ptr<IWarriorEnemy> MakeWarrior() override
        {
            return std::unique_ptr<IWarriorEnemy>(new CommonWarrior());
        }

        std::unique_ptr<IArcherEnemy> MakeArcher() override
        {
            return std::unique_ptr<IArcherEnemy>(new CommonArcher());
        }
    };

    class HardDifficultyEnemyFactory: public IEnemyFactory
    {
    public:
        std::unique_ptr<IWarriorEnemy> MakeWarrior() override
        {
            return std::unique_ptr<IWarriorEnemy>(new LegendaryWarrior());
        }

        std::unique_ptr<IArcherEnemy> MakeArcher() override
        {
            return std::unique_ptr<IArcherEnemy>(new LegendaryArcher());
        }
    };

};

#endif //PATTERNS_CPP_ABSTRACTFACTORY_H
