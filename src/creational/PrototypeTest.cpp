#include <gtest/gtest.h>
#include "Prototype.h"

using namespace Prototype;

TEST(Prototype, RecordKeepingExample)
{
    Employee employeePrototype
        {"", "", "Apriorit", "Knyazya Volodimira Velikogo 34b"};


    Employee ivan {"Ivan", "surname", employeePrototype};
    Employee alex {"Alex", "surname2", employeePrototype};

    EXPECT_EQ(ivan.companyAddress, alex.companyAddress);
    EXPECT_EQ(ivan.companyName, alex.companyName);
};

TEST(Prototype, FactoryExample)
{
    AprioritEmpoyeeFactory factory;
    Employee ivan = factory.MakeMainOfficeEmployee("Ivan", "surname");
    Employee alex = factory.MakeMainOfficeEmployee("Alex", "surname");

    Employee dennis = factory.MakeAuxOfficeEmployee("Dennis", "surname");
    Employee oleg = factory.MakeAuxOfficeEmployee("Oleg", "surname");

    EXPECT_EQ(ivan.companyAddress, alex.companyAddress);
    EXPECT_EQ(ivan.companyName, alex.companyName);

    EXPECT_EQ(dennis.companyName, oleg.companyName);
    EXPECT_EQ(dennis.companyName, oleg.companyName);
};
