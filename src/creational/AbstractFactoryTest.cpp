#include <gtest/gtest.h>
#include "AbstractFactory.h"

using namespace AbstractFactory;

namespace
{

    class GameSimulator
    {
    public:
        GameSimulator(std::unique_ptr<IEnemyFactory> enemyFactory)
                : enemyFactory_(std::move(enemyFactory))
        {
        }

    public:
        size_t TestFightTotalDamage()
        {
            auto archerPtr = enemyFactory_->MakeArcher();
            auto warriorPtr = enemyFactory_->MakeWarrior();

            return archerPtr->RangeAttack() + warriorPtr->MeleeAttack();
        }

    private:
        std::unique_ptr<IEnemyFactory> enemyFactory_;
    };
}

TEST(AbstractFactory, SimulateNormalDifficultyFight)
{
    GameSimulator gameSimulator(std::unique_ptr<IEnemyFactory>(
            new NormalDifficultyEnemyFactory()));

    const size_t totalDamage = gameSimulator.TestFightTotalDamage();
    EXPECT_EQ(totalDamage, CommonArcher::ATTACK + CommonWarrior::ATTACK);
}

TEST(AbstractFactory, SimulateHardDifficultyFight)
{
    GameSimulator gameSimulator(std::unique_ptr<IEnemyFactory>(
            new HardDifficultyEnemyFactory()));

    const size_t totalDamage = gameSimulator.TestFightTotalDamage();
    EXPECT_EQ(totalDamage, LegendaryArcher::ATTACK + LegendaryWarrior::ATTACK);
}

