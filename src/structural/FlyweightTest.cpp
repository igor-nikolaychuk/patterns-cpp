#include "Flyweight.h"
#include <gtest/gtest.h>

using namespace Flyweight;

TEST(Flyweight, UsageExample)
{
    TextureFactory textureFactory;

    auto t1 = textureFactory.GetTexture("t1");
    auto cachedT1 = textureFactory.GetTexture("t1");
    auto t2 = textureFactory.GetTexture("t2");

    EXPECT_EQ(t1.get(), cachedT1.get());
    EXPECT_NE(t2.get(), t1.get());
}