#include <gtest/gtest.h>
#include "CompositeWithGroovyStyleBuilder.h"

#include <sstream>

using namespace GroovyStyleBuilder;

TEST(GroovyStyleBuilder, SimpleHtml)
{
    Tag body{"body", "hello world",
         {
             P{"one more paragraph"},
             P{"and one more paragraph",
               {
                   P{"with a nested paragraph"}
               }
             }
         }
    };

    std::ostringstream html;
    html << body;
    std::string htmlStr = html.str();
    EXPECT_EQ(
        "<body>hello world<p>one more paragraph</p><p>and one more paragraph<p>with a nested paragraph</p></p></body>",
        htmlStr);
};


