#ifndef PATTERNS_CPP_FLYWEIGHT_H
#define PATTERNS_CPP_FLYWEIGHT_H

#include <vector>
#include <map>
#include <string>
#include <memory>

namespace Flyweight
{
    struct Texture
    {
        size_t h;
        size_t w;
        std::vector<uint8_t> Bytes;
    };

    class TextureFactory
    {
    public:
        std::shared_ptr<Texture> GetTexture(const std::string& name)
        {
            auto textureIt = textures_.find(name);
            if (textureIt == textures_.end() || textureIt->second.expired())
            {
                auto texture = LoadTexture(name);
                textures_[name] = texture;

                return texture;
            }

            return textureIt->second.lock();
        }

    private:
        static std::shared_ptr<Texture> LoadTexture(const std::string& name)
        {
            // Slow loading logic
            return std::make_shared<Texture>();
        }

    private:
        std::map<std::string, std::weak_ptr<Texture>> textures_;
    };
}

#endif //PATTERNS_CPP_FLYWEIGHT_H
