#include <gtest/gtest.h>
#include "Bridge.h"

using namespace Bridge;

TEST(Bridge, UsageExample)
{
    TVDevice tv;
    DisplayDevice display;

    StandardDeviceManupulator standardTvManipulator(tv);
    StandardDeviceManupulator standardDisplayManipulator(display);
    FastDeviceManipulator     fastTvManipulator(tv);
    FastDeviceManipulator     fastDisplayManipulator(display);

    /* IDevice and IDeviceManipulator hierarchies could grow independently,
     * there is not need to make additional class for each pair of IDevice
     * and IDeviceManipulator: StandardTVDeviceManipulator, FastTVDeviceManipulator ... */
};
