#ifndef PATTERNS_CPP_BRIDGE_H
#define PATTERNS_CPP_BRIDGE_H

#include <cstddef>

namespace Bridge
{
    class IDevice {
    public:
        virtual void SetVolume(size_t volume)   = 0;
        virtual void SetChannel(size_t channel) = 0;
        virtual size_t GetVolume()              = 0;
        virtual size_t GetChannel()             = 0;

        virtual ~IDevice() = default;
    };

    class IDeviceManipulator
    {
    public:
        virtual void VolumeUp()    = 0;
        virtual void VolumeDown()  = 0;
        virtual void ChannelUp()   = 0;
        virtual void ChannelDown() = 0;

        virtual ~IDeviceManipulator() = default;
    };


    class StandardDeviceManupulator
    {
    public:
        explicit StandardDeviceManupulator(IDevice& device)
                : device_(device)
        { }

        void VolumeUp()
        {
            device_.SetVolume(device_.GetVolume() + 1);
        }

        void VolumeDown()
        {
            device_.SetVolume(device_.GetVolume() - 1);
        }

        void ChannelUp()
        {
            device_.SetChannel(device_.GetChannel() + 1);
        }

        void ChannelDown()
        {
            device_.SetChannel(device_.GetChannel() - 1);
        }

    private:
        IDevice& device_;
    };

    class FastDeviceManipulator
    {
    public:
        explicit FastDeviceManipulator(IDevice& device)
            : device_(device)
        { }

        void VolumeUp()
        {
            device_.SetVolume(device_.GetVolume() + 10);
        }

        void VolumeDown()
        {
            device_.SetVolume(device_.GetVolume() + 10);
        }

        void ChannelUp()
        {
            device_.SetChannel(device_.GetChannel() + 1);
        }

        void ChannelDown()
        {
            device_.SetChannel(device_.GetChannel() - 1);
        }

    private:
        IDevice& device_;
    };

    class TVDevice: public IDevice
    {
    public:
        void SetVolume(size_t volume) override {
            volume_ = volume;
        }

        void SetChannel(size_t channel) override {
            channel_ = channel;
        }

        size_t GetVolume() override {
            return volume_;
        }

        size_t GetChannel() override {
            return channel_;
        }

    private:
        size_t volume_ = 0;
        size_t channel_ = 0;
    };

    class DisplayDevice: public IDevice
    {
    public:
        void SetVolume(size_t /* volume */) override {}

        void SetChannel(size_t channel) override {
            channel_ = channel;
        }

        size_t GetVolume() override {
            return 0;
        }

        size_t GetChannel() override {
            return channel_;
        }

    private:
        size_t channel_ = 0;
    };
}

#endif //PATTERNS_CPP_BRIDGE_H
