#ifndef PATTERNS_CPP_DECORATOR_H
#define PATTERNS_CPP_DECORATOR_H

#include <string_view>
#include <ostream>
#include <algorithm>

namespace Decorator
{
    class IOutputStream
    {
    public:
        virtual void Write(std::string_view data) = 0;
        virtual void Flush() = 0;

        virtual ~IOutputStream() = default;
    };

    class OutputStreamOStreamAdapter: public IOutputStream
    {
    public:
        explicit OutputStreamOStreamAdapter(std::ostream& out)
            : out_(out) {}

        void Write(std::string_view data) override
        {
            out_.write(data.data(), data.length());
        }

        void Flush() override
        {
            out_.flush();
        }

    private:
        std::ostream& out_;
    };

    class XorOutputStreamDecorator: public IOutputStream
    {
    public:
        XorOutputStreamDecorator(IOutputStream& nestedStream, uint8_t value)
        : nestedStream_(nestedStream), value_(value) {}

        void Write(std::string_view data) override
        {
            std::string modifiedData;
            modifiedData.resize(data.size());
            std::transform(data.begin(), data.end(), modifiedData.begin(),
                [this](char byte) -> char
                {
                    return byte ^ value_;
                });

            nestedStream_.Write(modifiedData);
        }

        void Flush()
        {
            nestedStream_.Flush();
        }

    private:
        IOutputStream& nestedStream_;
        uint8_t value_;
    };
}

#endif //PATTERNS_CPP_DECORATOR_H
