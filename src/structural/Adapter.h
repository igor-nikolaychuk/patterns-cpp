#ifndef PATTERNS_CPP_ADAPTER_H
#define PATTERNS_CPP_ADAPTER_H

#include <functional>

namespace Adapter
{
    struct Color
    {
        uint8_t r;
        uint8_t g;
        uint8_t b;
    };

    using PixelHandler =
            std::function<void(int x, int y, Color color)>;

    class IColorImage
    {
    public:
        virtual void Draw(PixelHandler handler) const = 0;
        virtual ~IColorImage() = default;
    };

    using GreyscalePixelHandler =
            std::function<void(int x, int y, uint8_t tone)>;

    class IGreyscaleImage
    {
    public:
        virtual void Draw(GreyscalePixelHandler handler) const = 0;
    };

    class ColorAsGreyscaleImageAdapter: public IGreyscaleImage
    {
    public:
        explicit ColorAsGreyscaleImageAdapter(const IColorImage& colorImage)
            : colorImage_(colorImage) { }

        void Draw(GreyscalePixelHandler handler) const override {
            return colorImage_.Draw([&](int x, int y, Color color){
                int avgColor = color.r + color.g + color.b;
                avgColor /= 3;

                handler(x, y, avgColor);
            });
        }

    private:
        const IColorImage& colorImage_;
    };
}

#endif //PATTERNS_CPP_ADAPTER_H
