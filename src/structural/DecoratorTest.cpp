#include "Decorator.h"
#include <gtest/gtest.h>

#include <sstream>

using namespace Decorator;

TEST(Decorator, UsageSample)
{
    std::ostringstream outputStream;

    OutputStreamOStreamAdapter outputAdapter(outputStream);
    XorOutputStreamDecorator xorDecryptor(outputAdapter, 0x66);
    XorOutputStreamDecorator xorEncryptor(xorDecryptor, 0x66);

    xorEncryptor.Write("Hello");
    xorEncryptor.Write(" world");

    EXPECT_EQ("Hello world", outputStream.str());
}
