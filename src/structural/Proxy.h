#ifndef PATTERNS_CPP_PROXY_H
#define PATTERNS_CPP_PROXY_H

#include <stdexcept>

namespace Proxy {
    class IBankAccount {
    public:
        virtual void Withdraw(double sum) = 0;

        virtual double Sum() const = 0;

        virtual ~IBankAccount() = default;
    };

    class ActualBankAccount: public IBankAccount {
    public:
        explicit ActualBankAccount(double sum)
            : sum_(sum) { }

        void Withdraw(double sum) override {
            sum_ -= sum;
        }

        double Sum() const override {
            return sum_;
        }

    private:
        double sum_;
    };

    class CreditLimiterBankAccountProxy : IBankAccount {
    public:
        explicit CreditLimiterBankAccountProxy(IBankAccount &account, double creditLimit)
                : account_(account), creditLimit_(creditLimit) {}

        void Withdraw(double sum) override {
            if (account_.Sum() - sum < -1 * creditLimit_) {
                throw std::runtime_error("Operation rejected: credit limit exceeded");
            }

            account_.Withdraw(sum);
        }

        double Sum() const override {
            return account_.Sum();
        }

    private:
        IBankAccount &account_;
        double creditLimit_;
    };
}

#endif //PATTERNS_CPP_PROXY_H