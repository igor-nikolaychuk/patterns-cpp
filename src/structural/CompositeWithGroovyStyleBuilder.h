#ifndef PATTERNS_CPP_COMPOSITEWITHGROOVYSTYLEBUILDER_H
#define PATTERNS_CPP_COMPOSITEWITHGROOVYSTYLEBUILDER_H

#include <string>
#include <vector>
#include <sstream>

namespace GroovyStyleBuilder
{
    class Tag
    {
    public:
        explicit Tag(std::string name, std::string text = "")
        : name_(std::move(name)), text_(std::move(text))
        {}

        Tag(
                std::string name,
                std::string text,
                std::vector<Tag> children
        )
                : name_(std::move(name)), text_(std::move(text)), children_(std::move(children))
        {}

        Tag(
                std::string name,
                std::vector<Tag> children
        )
                : name_(std::move(name)), children_(std::move(children))
        {}

        friend std::ostream& operator<<(std::ostream& os, const Tag& tag)
        {
            os << '<' << tag.name_ << '>';
            os << tag.text_;
            for (const auto& child: tag.children_)
            {
                os << child;
            }
            os << "</" << tag.name_ << '>';

            return os;
        }

    private:
        std::string name_;
        std::string text_;
        std::vector<Tag> children_;
    };

    class P: public Tag
    {
    public:
        explicit P(std::string text = "", std::vector<Tag> children = {}):
                Tag("p", std::move(text), std::move(children)) {}
    };


};

#endif //PATTERNS_CPP_COMPOSITEWITHGROOVYSTYLEBUILDER_H
