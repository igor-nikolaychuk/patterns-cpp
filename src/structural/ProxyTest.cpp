#include "Proxy.h"
#include <gtest/gtest.h>

using namespace Proxy;

TEST(Proxy, UsageSample)
{
    ActualBankAccount account(100.);
    CreditLimiterBankAccountProxy creditLimiter(account, 50.);

    creditLimiter.Withdraw(140.);
    EXPECT_EQ(creditLimiter.Sum(), -40.);
    EXPECT_THROW(creditLimiter.Withdraw(20), std::runtime_error);
}
