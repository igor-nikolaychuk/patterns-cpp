#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "Adapter.h"

using namespace Adapter;
using namespace testing;

class SinglePixelImage: public IColorImage
{
public:
    void Draw(PixelHandler handler) const override {
        handler(0, 0, {0, 50, 100});
    }
};

using GreyscalePixelHandlerMock = MockFunction<void(int x, int y, uint8_t tone)>;

TEST(Adapter, UsageExample)
{
    SinglePixelImage original;
    GreyscalePixelHandlerMock greyscalePixelHandler;
    EXPECT_CALL(greyscalePixelHandler, Call(0, 0, uint8_t(50)))
        .Times(1);

    ColorAsGreyscaleImageAdapter adapter(original);
    adapter.Draw(greyscalePixelHandler.AsStdFunction());
};
